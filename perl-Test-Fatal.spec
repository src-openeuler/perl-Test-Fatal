Name:           perl-Test-Fatal
Version:        0.017
Release:        1
Summary:        Incredibly simple helpers for testing code with exceptions
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Test-Fatal
Source0:        https://cpan.metacpan.org/authors/id/R/RJ/RJBS/Test-Fatal-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  perl-interpreter, perl-generators, perl(ExtUtils::MakeMaker)
BuildRequires:  perl(Carp), perl(Exporter) >= 5.57, perl(strict), perl(Test::Builder)
BuildRequires:  perl(Try::Tiny) >= 0.07, perl(warnings), perl(File::Spec), perl(overload)
BuildRequires:  perl(Test::Builder::Tester), perl(Test::More) >= 0.96
Requires:       perl(Test::Builder)

%description
Test::Fatal is an alternative to the popular Test::Exception.
It does much less, but should allow greater flexibility in
testing exception-throwing code with about the same amount
of typing.
It exports one routine by default: exception.

%package_help

%prep
%autosetup -n Test-Fatal-%{version} -p1
chmod -c -x examples/*

%build
perl Makefile.PL INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT

%check
make test

%files
%license LICENSE
%doc README examples/
%{perl_vendorlib}/Test/

%files help
%{_mandir}/man*/*

%changelog
* Thu Feb 27 2025  openeuler_bot <infra@openeuler.sh> - 0.017-1
- no changes since trial release of 0.015 (version:0.016)
- update packaging and metadata (version:0.017)
- add default descriptions to tests (version:0.015)
- work on ancient Test::Builder code (version:0.015)

* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 0.014-14
- drop useless perl(:MODULE_COMPAT) requirement

* Mon Jan 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.014-13
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:change delete packlist

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.014-12
- Package init
